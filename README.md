### BlueToothScanner

### Summary

Bluetooth Scanner is app that shows information about nearby bluetooth device.

### Features

1. Scan nearby bluetooth devices
2. Show peripherals information
3. Filter by RSSI range
4. User profile

### Architecture

1. Clean Architecture
2. MVVM design pattern
3. Unit test
-  LaunchActivity: LaunchViewModel: 100%
-  MainActivity: MainViewModel: 43%  , DeviceRepositoryImp: 100%
-  UserProfileActivity: UserProfileViewModel: 80% , UserProfileRepositoryImp: 100%
4. Mockito for unit testing

### Installation and usage

Real devices with Android 5.0 Lollipop +( Core bluetooth won't work on simulator )

// GrapQl

 cd into D:\Project\ProjectPersonal\mybluetooth\app\src\main\graphql folder
 run cmd/termial : apollo-codegen download-schema https://api.github.com/graphql --output schema.json --header "Authorization: Bearer 207b51602ea5333505915cdc9382bba73ce0825f"
 with author "207b51602ea5333505915cdc9382bba73ce0825f" is generate token your github
 after run above statement schema.json will automatically generated
