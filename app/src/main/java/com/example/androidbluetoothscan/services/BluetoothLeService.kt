package com.example.androidbluetoothscan.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.ui.view.DashboardActivity
import com.example.androidbluetoothscan.utils.Constant.CHANNEL_NAME
import com.example.androidbluetoothscan.utils.Constant.NOTIFICATION_CHANNEL_ID
import com.example.androidbluetoothscan.utils.Constant.TICKER_NOTIFY
import com.example.androidbluetoothscan.utils.getDeviceBean
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
class BluetoothLeService : Service() {

    @Inject
    lateinit var deviceUseCase: DeviceUseCase

    private val scanBluetoothBinder = ScanBluetoothBinder()
    private lateinit var bluetoothManager: BluetoothManager
    private lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var bluetoothLeScanner: BluetoothLeScanner
    private lateinit var handlerHandleScanning: Handler
    private lateinit var runnableHandleScanning: Runnable
    private var isScanning = false
    private var bluetoothDevices = mutableListOf<DeviceBluetooth>()

    // Stops scanning after 10 seconds.
    companion object {
        private val SCAN_PERIOD: Long = 500000 //5m
        private val STOP_PERIOD: Long = 10000 //10s
    }

    var needEnableBluetoothCallBack: ((Boolean) -> Unit)? = null
    var changeDataCallBack: ((MutableList<DeviceBluetooth>) -> Unit)? = null
    var statusScanCallBack: ((Boolean) -> Unit)? = null
    var statusSupportBluetoothCallBack: ((Boolean) -> Unit)? = null

    private var devices = mutableListOf<DeviceBluetooth>()

    //coroutine
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    // Device scan callback.
    private val leScanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            scope.launch(Dispatchers.IO) {
                val deviceBluetooth = result.getDeviceBean()
                deviceBluetooth.timeStamp = System.currentTimeMillis() / 1000
                deviceUseCase.insertDevice(deviceBluetooth)
                checkExitBluetoothAroundHere(deviceBluetooth)
                changeDataCallBack?.invoke(bluetoothDevices)
            }.invokeOnCompletion {
                scope.launch(Dispatchers.IO) {
                    devices = deviceUseCase.getDevices()
                }
            }
        }
    }

    private fun checkExitBluetoothAroundHere(deviceBluetooth: DeviceBluetooth) {
        if (bluetoothDevices.isNullOrEmpty()) {
            bluetoothDevices.add(deviceBluetooth)
        } else {
            try {
                val itemExist =
                    bluetoothDevices.filter { item -> item.address == deviceBluetooth.address }
                if (itemExist.isNullOrEmpty()) {
                    bluetoothDevices.add(deviceBluetooth)
                }
            } catch (e: Exception) {
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        if (bluetoothAdapter == null) {
            statusSupportBluetoothCallBack?.invoke(false)
        } else {
            statusSupportBluetoothCallBack?.invoke(true)
            if (!bluetoothAdapter.isEnabled) {
                needEnableBluetoothCallBack?.invoke(true)
            } else {
                bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner
                handlerHandleScanning = Handler()
                runnableHandleScanning = Runnable {
                    if (!isScanning) {
                        startScan()
                        handlerHandleScanning.postDelayed(runnableHandleScanning, SCAN_PERIOD)
                    } else {
                        stopScan()
                        handlerHandleScanning.postDelayed(runnableHandleScanning, STOP_PERIOD)
                    }
                }

            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return scanBluetoothBinder
    }

    inner class ScanBluetoothBinder : Binder() {
        fun getScanBluetoothService(): BluetoothLeService {
            return this@BluetoothLeService
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        parentJob.cancel()
        stopSelf()
    }

    fun startScan() {
        bluetoothAdapter?.run {
            if (!this.isEnabled) {
                //show dialog requesting user permission to enable Bluetooth
                needEnableBluetoothCallBack?.invoke(true)
                return
            }

            bluetoothDevices = mutableListOf()
            scope.launch(Dispatchers.IO) {
//                deviceUseCase.removeDevices()
            }.invokeOnCompletion {
                runnableHandleScanning.run()
                isScanning = true
                bluetoothLeScanner.startScan(leScanCallback)
                statusScanCallBack?.invoke(isScanning)
            }
        }
    }

    fun stopScan() {
        bluetoothAdapter?.run {
            if (!this.isEnabled) {
                needEnableBluetoothCallBack?.invoke(true)
                return
            }
            isScanning = false
            bluetoothLeScanner.stopScan(leScanCallback)
            handlerHandleScanning.removeCallbacks(runnableHandleScanning)
            statusScanCallBack?.invoke(isScanning)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//            startMyOwnForeground()
//        else
//            startForeground(1, Notification())
        return START_STICKY
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            CHANNEL_NAME,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val intent = Intent(this, DashboardActivity::class.java)
        val pIntent =
            PendingIntent.getActivity(this, System.currentTimeMillis().toInt(), intent, 0)

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan)

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true)
            .setTicker(TICKER_NOTIFY)
            .setContentIntent(pIntent)
            .setPriority(NotificationManager.IMPORTANCE_HIGH)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(2, notification)
    }

    override fun onDestroy() {
        handlerHandleScanning.removeCallbacks(runnableHandleScanning)
        super.onDestroy()
    }
}