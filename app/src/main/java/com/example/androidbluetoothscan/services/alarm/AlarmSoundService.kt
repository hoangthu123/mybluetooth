package com.example.androidbluetoothscan.services.alarm

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.util.Log
import com.example.androidbluetoothscan.R

class AlarmSoundService : Service() {
    private var mediaPlayer: MediaPlayer? = null
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        //Start media player
        mediaPlayer = MediaPlayer.create(this,R.raw.butt_buddy)
        mediaPlayer!!.start()
        mediaPlayer!!.isLooping = true //set looping true to run it infinitely
        Log.d("Alarm",AlarmSoundService::class.java.name)
    }

    override fun onDestroy() {
        super.onDestroy()

        //On destory stop and release the media player
        if (mediaPlayer != null && mediaPlayer!!.isPlaying) {
            mediaPlayer!!.stop()
            mediaPlayer!!.reset()
            mediaPlayer!!.release()
        }
    }
}