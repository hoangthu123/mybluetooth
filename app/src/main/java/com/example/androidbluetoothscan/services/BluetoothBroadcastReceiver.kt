package com.example.androidbluetoothscan.services

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BluetoothBroadcastReceiver : BroadcastReceiver() {

    var statusBluetoothChange: ((Boolean) -> Unit)? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action

        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            when (intent?.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {

                BluetoothAdapter.STATE_OFF -> statusBluetoothChange?.invoke(false)
                BluetoothAdapter.STATE_ON -> statusBluetoothChange?.invoke(true)
                else -> return

            }

        }
    }
}