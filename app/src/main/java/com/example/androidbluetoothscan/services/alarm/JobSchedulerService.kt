package com.example.androidbluetoothscan.services.alarm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.JobIntentService
import androidx.core.app.NotificationCompat
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.ui.view.DashboardActivity

class JobSchedulerService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {

        sendNotification("Wake Up! Wake Up! Alarm started!!")
        Log.d("Alarm",JobSchedulerService::class.java.name)

    }

    companion object {
        const val JOB_ID = 1000

        //Notification ID for Alarm
        const val NOTIFICATION_ID = 1

        @JvmStatic
        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(
                context,
                JobSchedulerService::class.java,
                JOB_ID,
                work
            )
        }
    }


    private fun sendNotification(message: String) {

        val mBuilder =
            NotificationCompat.Builder(
                applicationContext,
                "notify_001"
            )
        val ii = Intent(applicationContext, DashboardActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, ii, 0)

        val bigText =
            NotificationCompat.BigTextStyle()
        bigText.bigText(message)
        bigText.setBigContentTitle("Today's Bible Verse")
        bigText.setSummaryText("Text in detail")

        mBuilder.setContentIntent(pendingIntent)
        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round)
        mBuilder.setContentTitle("Your Title")
        mBuilder.setContentText("Your text")
        mBuilder.priority = Notification.PRIORITY_MAX
        mBuilder.setStyle(bigText)

        val mNotificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

// === Removed some obsoletes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "Your_channel_id"
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            mNotificationManager.createNotificationChannel(channel)
            mBuilder.setChannelId(channelId)
        }

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build())
    }

}