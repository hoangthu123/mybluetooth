package com.example.androidbluetoothscan.di.module

import android.content.Context
import androidx.annotation.NonNull
import com.apollographql.apollo.ApolloClient
import com.example.androidbluetoothscan.data.repository.DeviceRepositoryImp
import com.example.androidbluetoothscan.data.repository.GraphQleRepositoryImp
import com.example.androidbluetoothscan.data.repository.UserProfileRepositoryImp
import com.example.androidbluetoothscan.data.source.local.AppDatabase
import com.example.androidbluetoothscan.data.source.remote.ApiService
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import com.example.androidbluetoothscan.domain.repository.GraphQlRepository
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.domain.usecase.UserProfileUseCase
import com.example.androidbluetoothscan.ui.viewmodel.UserProfileViewModel
import com.example.androidbluetoothscan.utils.Constant.BASE_URL
import com.example.androidbluetoothscan.utils.Constant.BASE_URL_GRAPH_QL
import com.example.androidbluetoothscan.utils.Utils
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@InstallIn(ApplicationComponent::class)
@Module
//@Module(includes = [ApplicationModule::class])
class NetworkModule {

    @Provides
    @Singleton
    fun providesRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(
        @ApplicationContext context: Context,
        isNetworkAvailable: Boolean
    ): OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val mCache = Cache(context.cacheDir, cacheSize)
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .cache(mCache)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addNetworkInterceptor(interceptor)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (isNetworkAvailable) request.newBuilder()
                    .header("Cache-Control", "public, max-age=" + 5).build()
                else request.newBuilder().header(
                    "Cache-Control",
                    "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                ).build()
                chain.proceed(request)
            }
        return client.build()
    }


    @Provides
    @Singleton
    fun providesGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun providesRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideIsNetworkAvailable(@ApplicationContext context: Context): Boolean {
        return Utils.isNetworkAvailable(context)
    }

    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideDeviceRepository(appDatabase: AppDatabase): DeviceRepository {
        return DeviceRepositoryImp(
            appDatabase
        )
    }

    @Singleton
    @Provides
    fun provideAlbumRepository(
        retrofitService: ApiService
    ): UserProfileRepository {
        return UserProfileRepositoryImp(retrofitService)
    }

    @Singleton
    @Provides
    fun provideGraphQleRepositoryImp(
        apolloClient: ApolloClient
    ): GraphQlRepository {
        return GraphQleRepositoryImp(apolloClient)
    }

    @Singleton
    @Provides
    fun provideDeviceUseCase(repository: DeviceRepository) =
        DeviceUseCase(
            repository
        )

    @Provides
    @Singleton
    fun provideApolloClient(okHttpClient: OkHttpClient): ApolloClient {
        return ApolloClient.builder()
            .serverUrl(BASE_URL_GRAPH_QL)
            .okHttpClient(okHttpClient.newBuilder().addInterceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder().method(
                    original.method(),
                    original.body()
                )
                builder.addHeader(
                    "Authorization"
                    , "Bearer " + "207b51602ea5333505915cdc9382bba73ce0825f"
                )
                chain.proceed(builder.build())
            }
                .build())
            .build()
    }

}