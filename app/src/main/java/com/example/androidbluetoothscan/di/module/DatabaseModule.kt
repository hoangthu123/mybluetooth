package com.example.androidbluetoothscan.di.module

import android.app.Application
import androidx.room.Room
import com.example.androidbluetoothscan.data.source.local.AppDatabase
import com.example.androidbluetoothscan.data.source.local.dao.DeviceDao
import com.example.androidbluetoothscan.utils.DatabaseConstant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    internal fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            DatabaseConstant.DATABASE_NAME
        ).allowMainThreadQueries().build()
    }

    @Provides
    internal fun providePhotoDao(appDatabase: AppDatabase): DeviceDao {
        return appDatabase.deviceDao
    }
}