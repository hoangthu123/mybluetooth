package com.example.androidbluetoothscan.utils

object DatabaseConstant {

    const val AMENITY_BLUETOOTH_DEVICE = "device"
    const val DATABASE_NAME = "BluetoothDevice"
    const val DATABASE_VERSION = 2

}