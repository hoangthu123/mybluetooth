package com.example.androidbluetoothscan.utils

object Constant {

    const val BASE_URL = "https://anypoint.mulesoft.com/"
    const val BASE_URL_GRAPH_QL = "https://api.github.com/graphql"

    const val UN_KNOW = "Unknown"
    const val AUDIO_VIDEO = "AUDIO_VIDEO"
    const val COMPUTER = "COMPUTER"
    const val HEALTH = "HEALTH"
    const val IMAGING = "IMAGING"
    const val MISC = "MISC"
    const val NETWORKING = "NETWORKING"
    const val PERIPHERAL = "PERIPHERAL"
    const val PHONE = "PHONE"
    const val TOY = "TOY"
    const val UNCATEGORIZED = "UNCATEGORIZED"
    const val CLASSIC = "Classic"
    const val LE = "LE (Low Energy)"
    const val DUAL = "Dual"
    const val NOT_BONDED = "Not Bonded"
    const val BONDING = "Bonding.."
    const val BONDED = "Bonded"
    const val DBM = "dBm"

    const val NOTIFICATION_CHANNEL_ID = "example.permanence"
    const val CHANNEL_NAME = "Background Service"
    const val TICKER_NOTIFY = "App is running in background"

}