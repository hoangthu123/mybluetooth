package com.example.androidbluetoothscan.utils

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.androidbluetoothscan.R

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("app:loadAvatarFromUri")
    fun loadAvatarFromUri(imageView: ImageView, url: String?) {
        imageView.loadAvatarFromUri(url, R.mipmap.ic_launcher_round)
    }

    @JvmStatic
    @BindingAdapter("android:htmlText")
    fun setHtmlTextValue(textView: TextView, htmlText: String?) {
        if (htmlText == null) return
        val result: Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(htmlText)
        }
        textView.text = result
    }
}