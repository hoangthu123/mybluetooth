package com.example.androidbluetoothscan.utils

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController

fun Fragment.navigate(@IdRes resId: Int?) {
    val (resId) = guardLet(resId) {
        return
    }
    findNavController(this).navigate(resId)
}

fun Fragment.navigate(@IdRes resId: Int?, bundle: Bundle) {
    val (resId) = guardLet(resId) {
        return
    }
    findNavController(this).navigate(resId,bundle)
}

fun Fragment.getCurrentNavigate(): Int {
   return findNavController(this).currentDestination?.id?:0
}

fun Fragment.navigateUp(){
    findNavController(this).navigateUp()
}

