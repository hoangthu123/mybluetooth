package com.example.androidbluetoothscan.utils

import android.bluetooth.le.ScanResult
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth

fun ScanResult.getDeviceBean(): DeviceBluetooth {
    val device = DeviceBluetooth()
    device.address = this.device.address
    device.name = this.device.name
    device.major = this.device.bluetoothClass.majorDeviceClass
    device.type = this.device.type
    device.bondState = this.device.bondState
    device.rssi = this.rssi
    return device
}

fun ImageView.loadAvatarFromUri(uri: String?, defaultResId: Int) {
    val (uri) = guardLet(uri) {
        this.setImageResource(R.mipmap.ic_launcher_round)
        return
    }

    Glide.with(this.context)
        .asBitmap()
        .apply(RequestOptions.circleCropTransform())
        .placeholder(defaultResId)
        .load(uri)
        .into(this)

}

inline fun <T : Any> guardLet(vararg elements: T?, closure: () -> Nothing): List<T> {
    return if (elements.all { it != null }) {
        elements.filterNotNull()
    } else {
        closure()
    }
}

