package com.example.androidbluetoothscan.utils

object BundleConstant {

    const val MESSAGE_DIALOG = "Message"
    const val URL_DIALOG = "UrlDialog"

}