package com.example.androidbluetoothscan.domain.repository

import com.example.androidbluetoothscan.domain.model.DeviceBluetooth

interface DeviceRepository {

    fun insertDevice(deviceBluetooth: DeviceBluetooth)

    fun getDevicesNormal(): MutableList<DeviceBluetooth>
}