package com.example.androidbluetoothscan.domain.model

class Address(
    var building: String? = null,
    var city: String? = null,
    var geo: Geo? = null,
    var street: String? = null,
    var unit: String? = null,
    var zipcode: String? = null
) {

    val formatAddress: String
        get() {
            return "$building, $street, $city "
        }
}