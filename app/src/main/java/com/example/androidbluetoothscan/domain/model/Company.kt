package com.example.androidbluetoothscan.domain.model

data class Company(
    var name: String? = null
)