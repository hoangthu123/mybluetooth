package com.example.androidbluetoothscan.domain.usecase

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.exception.ApolloException
import com.example.androidbluetoothscan.FindQuery
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.utils.Constant
import okhttp3.OkHttpClient
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


abstract class MainQraphQlUseCase<T>  {

    private lateinit var client: ApolloClient

    suspend fun execute():String{
        return suspendCoroutine { continuation ->
            client = setupApollo()
            client.query(
                FindQuery    //From the auto generated class
                    .builder()
                    .name("S3_AndroidBluetoothScan_ThuHoang") //Passing required arguments
                    .owner("s3corp-github") //Passing required arguments
                    .build())
                .enqueue(object : ApolloCall.Callback<FindQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        continuation.resumeWithException(e)
                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<FindQuery.Data>) {
                        continuation.resume(response.data()?.repository().toString())
                    }
                })


        }

    }


    private fun setupApollo(): ApolloClient {
        val okHttp = OkHttpClient
            .Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder().method(original.method(),
                    original.body())
                builder.addHeader("Authorization"
                    , "Bearer " + "207b51602ea5333505915cdc9382bba73ce0825f")
                chain.proceed(builder.build())
            }
            .build()
        return ApolloClient.builder()
            .serverUrl(Constant.BASE_URL_GRAPH_QL)
            .okHttpClient(okHttp)
            .build()
    }


}

