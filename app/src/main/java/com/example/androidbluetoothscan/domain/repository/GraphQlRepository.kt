package com.example.androidbluetoothscan.domain.repository

interface GraphQlRepository {

    suspend fun getDataGraphQl(): String
}