package com.example.androidbluetoothscan.domain.usecase

import com.example.androidbluetoothscan.domain.repository.GraphQlRepository
import javax.inject.Inject

class GraphQlUseCase @Inject constructor(private val repository: GraphQlRepository) {

     suspend fun getDataGraphQl(): String {
        return repository.getDataGraphQl()
    }
}