package com.example.androidbluetoothscan.domain.model

import android.bluetooth.BluetoothClass
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.androidbluetoothscan.utils.DatabaseConstant
import com.example.androidbluetoothscan.utils.Constant.AUDIO_VIDEO
import com.example.androidbluetoothscan.utils.Constant.BONDED
import com.example.androidbluetoothscan.utils.Constant.BONDING
import com.example.androidbluetoothscan.utils.Constant.CLASSIC
import com.example.androidbluetoothscan.utils.Constant.COMPUTER
import com.example.androidbluetoothscan.utils.Constant.DBM
import com.example.androidbluetoothscan.utils.Constant.DUAL
import com.example.androidbluetoothscan.utils.Constant.HEALTH
import com.example.androidbluetoothscan.utils.Constant.IMAGING
import com.example.androidbluetoothscan.utils.Constant.LE
import com.example.androidbluetoothscan.utils.Constant.MISC
import com.example.androidbluetoothscan.utils.Constant.NETWORKING
import com.example.androidbluetoothscan.utils.Constant.NOT_BONDED
import com.example.androidbluetoothscan.utils.Constant.PERIPHERAL
import com.example.androidbluetoothscan.utils.Constant.PHONE
import com.example.androidbluetoothscan.utils.Constant.TOY
import com.example.androidbluetoothscan.utils.Constant.UNCATEGORIZED
import com.example.androidbluetoothscan.utils.Constant.UN_KNOW
import com.example.androidbluetoothscan.utils.Utils

@Entity(tableName = "${DatabaseConstant.AMENITY_BLUETOOTH_DEVICE}")
data class DeviceBluetooth(
    var id: Int = 1,
    var name: String? = "",
    @PrimaryKey
    var address: String = "",
    var major: Int? = -1,
    var type: Int? = -1,
    var bondState: Int? = -1,
    var rssi: Int? = 0,
    var timeStamp: Long = 0L
) {

    val formatBluetoothClass: String
        get() {
            return when (major) {
                BluetoothClass.Device.Major.AUDIO_VIDEO,
                BluetoothClass.Device.Major.WEARABLE -> AUDIO_VIDEO
                BluetoothClass.Device.Major.COMPUTER -> COMPUTER
                BluetoothClass.Device.Major.HEALTH -> HEALTH
                BluetoothClass.Device.Major.IMAGING -> IMAGING
                BluetoothClass.Device.Major.MISC -> MISC
                BluetoothClass.Device.Major.NETWORKING -> NETWORKING
                BluetoothClass.Device.Major.PERIPHERAL -> PERIPHERAL
                BluetoothClass.Device.Major.PHONE -> PHONE
                BluetoothClass.Device.Major.TOY -> TOY
                BluetoothClass.Device.Major.UNCATEGORIZED -> UNCATEGORIZED
                else -> UN_KNOW
            }
        }

    val formatBluetoothType: String
        get() {
            return when (type) {
                1 -> CLASSIC
                2 -> LE
                3 -> DUAL
                else -> UN_KNOW
            }
        }

    val formatBondState: String
        get() {
            return when (bondState) {
                10 -> NOT_BONDED
                11 -> BONDING
                12 -> BONDED
                else -> UN_KNOW
            }
        }

    val formatRssi: String
        get() {
            return "$rssi $DBM"
        }

    val formatTimeStamp: String
        get() {
            return Utils.getDate(timeStamp)
        }
}