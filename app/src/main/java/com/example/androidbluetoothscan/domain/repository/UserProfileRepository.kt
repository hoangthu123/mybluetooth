package com.example.androidbluetoothscan.domain.repository

import com.example.androidbluetoothscan.domain.model.User
import io.reactivex.Single

interface UserProfileRepository {

    fun getUserProfile(): Single<User>
}