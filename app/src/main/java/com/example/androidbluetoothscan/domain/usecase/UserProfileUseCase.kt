package com.example.androidbluetoothscan.domain.usecase

import com.example.androidbluetoothscan.domain.model.User
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import io.reactivex.Single
import javax.inject.Inject

class UserProfileUseCase @Inject constructor(private val repository: UserProfileRepository) :
    SingleUseCase<User>() {

    override fun buildUseCaseSingle(): Single<User> {
        return repository.getUserProfile()
    }
}