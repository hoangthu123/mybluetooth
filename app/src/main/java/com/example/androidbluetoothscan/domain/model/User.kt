package com.example.androidbluetoothscan.domain.model

data class User(
    var address: Address? = null,
    var avatar: String? = null,
    var company: Company? = null,
    var email: String? = null,
    var id: Int? = null,
    var name: String? = null,
    var phone: String? = null,
    var username: String? = null,
    var website: String? = null
) {

    val formatWebsite: String
        get() {
            return "<a href=\"$website\">$website</a>"
        }
}

