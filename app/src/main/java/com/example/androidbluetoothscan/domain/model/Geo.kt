package com.example.androidbluetoothscan.domain.model

data class Geo(
    var lat: String? = null,
    var lng: String? = null
)