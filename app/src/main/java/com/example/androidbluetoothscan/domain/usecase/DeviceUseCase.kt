package com.example.androidbluetoothscan.domain.usecase

import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeviceUseCase @Inject constructor( val repository: DeviceRepository) {

    fun insertDevice(deviceBluetooth: DeviceBluetooth) {
        repository.insertDevice(deviceBluetooth)

    }

    fun getDevices(): MutableList<DeviceBluetooth> {
        return repository.getDevicesNormal()
    }
}