package com.example.androidbluetoothscan.ui.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentOpenUrlDialogBinding
import com.example.androidbluetoothscan.utils.navigateUp

class OpenUrlDialogFragment : Fragment() {

    private lateinit var binding: FragmentOpenUrlDialogBinding

    companion object {
        const val KEY_URL = "key_url"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_url_dialog, container, false)

        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        setListener()
    }

    private fun setListener() {
        binding.btnClose.setOnClickListener { navigateUp() }
    }

    private fun initView() {
        arguments?.getString(KEY_URL)?.run {
            binding.wvUrl.webViewClient = WebViewClient()
            binding.wvUrl.settings.javaScriptEnabled = true;
            binding.wvUrl.loadUrl(this)
            binding.wvUrl.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView, request: WebResourceRequest
                ): Boolean {
                    view.loadUrl(request.url.toString())
                    return true
                }

                override fun onPageFinished(view: WebView, url: String) {
                    binding.pbLoading.visibility = View.GONE
                }
            }
        }

    }

}