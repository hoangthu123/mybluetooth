package com.example.androidbluetoothscan.ui.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentGraphQlBinding
import com.example.androidbluetoothscan.ui.viewmodel.GraphQlViewModel
import com.example.androidbluetoothscan.utils.guardLet
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GraphQlFragment : Fragment() {

    private lateinit var binding: FragmentGraphQlBinding
    private val viewModel: GraphQlViewModel
            by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_graph_ql, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.data.observe(viewLifecycleOwner, Observer {
            val (apiError) = guardLet(it) {
                return@Observer
            }

            Toast.makeText(requireContext(), apiError, Toast.LENGTH_SHORT).show()
        })
        viewModel.loadData()
    }

}