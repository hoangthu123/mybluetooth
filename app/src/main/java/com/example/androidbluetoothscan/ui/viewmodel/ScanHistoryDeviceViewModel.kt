package com.example.androidbluetoothscan.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase

class ScanHistoryDeviceViewModel @ViewModelInject constructor(
    deviceUseCase: DeviceUseCase
) : ViewModel() {

    private var _devices: MutableLiveData<MutableList<DeviceBluetooth>> = MutableLiveData()
    val devices: LiveData<MutableList<DeviceBluetooth>> get() = _devices

    private var _numberDevice = MutableLiveData<String>()
    val numberDevice: LiveData<String> get() = _numberDevice

    private var _progressRssi = MutableLiveData<String>()
    val progressRssi: LiveData<String> get() = _progressRssi

    init {
        _devices.postValue(
            deviceUseCase.getDevices().sortedByDescending { it.timeStamp }
                .toMutableList())
    }

    fun setNumberDevice(value: String) {
        _numberDevice.value = value
    }

    fun setProgressRssiValue(value: String) {
        _progressRssi.value = value
    }
}