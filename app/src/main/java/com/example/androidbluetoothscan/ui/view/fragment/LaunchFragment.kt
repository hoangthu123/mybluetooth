package com.example.androidbluetoothscan.ui.view.fragment

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentLaunchBinding
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.ui.dialog.MessageDialogFragment
import com.example.androidbluetoothscan.ui.view.DashboardActivity
import com.example.androidbluetoothscan.ui.viewmodel.LaunchViewModel
import com.example.androidbluetoothscan.utils.getCurrentNavigate
import com.example.androidbluetoothscan.utils.navigate
import javax.inject.Inject

class LaunchFragment : Fragment() {


    @Inject
    lateinit var deviceUseCase: DeviceUseCase

    companion object{
        private val REQUEST_ENABLE_BT = 1
        private val PERMISSIONS_REQUEST_LOCATION = 2
    }

    private var bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private lateinit var binding: FragmentLaunchBinding
    private val viewModel: LaunchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_launch, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        observeData()
        checkLocationPermission()
    }


    private fun observeData() {
        viewModel.statusChangeBluetooth.observe(viewLifecycleOwner, Observer {
            if (it)
                openDiscoveryBluetoothScreen()
            else {
                bluetoothAdapter?.run {
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                }
            }
        })
    }

    private fun initView() {
        (activity as DashboardActivity).bluetoothBroadcastReceiver.statusBluetoothChange={
            viewModel.setStatusChangeBluetooth(it)
        }
    }

    private fun checkStateBluetooth() {
        if (bluetoothAdapter != null) {
            viewModel.setStatusChangeBluetooth(bluetoothAdapter.isEnabled)
        } else {
            val postalFragment =
                MessageDialogFragment().newInstance(getString(R.string.mess_not_support_bluetooth))
            postalFragment?.show(requireActivity().supportFragmentManager, MessageDialogFragment::class.java.name)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode ==REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            viewModel.setStatusChangeBluetooth(false)
        }

    }

    private fun openDiscoveryBluetoothScreen() {
        if (getCurrentNavigate() == R.id.test1Fragment)
            navigate(R.id.action_test1Fragment_to_test2Fragment)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {

        if (requestCode ==PERMISSIONS_REQUEST_LOCATION) {
            when (grantResults.first()) {
                PackageManager.PERMISSION_GRANTED -> checkStateBluetooth()
                else -> requestLocation()
            }
        }
    }

    private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(requireActivity(),  Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(requireActivity(),  Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED
        ) {
            checkStateBluetooth()
        } else requestLocation()
    }

    private fun requestLocation() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION ,
                Manifest.permission.ACCESS_FINE_LOCATION ),
           PERMISSIONS_REQUEST_LOCATION
        )
    }

}