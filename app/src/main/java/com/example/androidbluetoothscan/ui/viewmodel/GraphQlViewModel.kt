package com.example.androidbluetoothscan.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidbluetoothscan.domain.usecase.GraphQlUseCase
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class GraphQlViewModel @ViewModelInject constructor(private val graphQlUseCase: GraphQlUseCase) :
    ViewModel() {

    private var _data = MutableLiveData<String>()
    val data: LiveData<String> get() = _data

    private var _isLoad = MutableLiveData<Boolean>()
    val isLoad: LiveData<Boolean> get() = _isLoad

    //coroutine
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    init {
        _isLoad.value = true
    }

    fun loadData() {
        runBlocking { scope.launch(Dispatchers.Main) {
            graphQlUseCase.getDataGraphQl().also { session ->
                _data.value = session
                _isLoad.value = false
            }
        } }
    }

}