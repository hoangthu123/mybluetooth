package com.example.androidbluetoothscan.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidbluetoothscan.domain.model.User
import com.example.androidbluetoothscan.domain.usecase.UserProfileUseCase

class UserProfileViewModel @ViewModelInject constructor(private val getUserProfileUseCase: UserProfileUseCase) :
    ViewModel() {

    private var _userProfile = MutableLiveData<User>()
    val userProfile: LiveData<User> get() = _userProfile

    private var _isLoad = MutableLiveData<Boolean>()
    val isLoad: LiveData<Boolean> get() = _isLoad

    private var _error = MutableLiveData<Throwable>()
    val error: LiveData<Throwable> get() = _error

    init {
        _isLoad.value = true
    }

    fun getUserProfile() {
        getUserProfileUseCase.execute(
            onSuccess = {
                _isLoad.value = false
                _userProfile.value = it
            },
            onError = {
                _isLoad.value = false
                _error.value = it
            }
        )
    }

}