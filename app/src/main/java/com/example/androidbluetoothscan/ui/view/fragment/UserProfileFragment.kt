package com.example.androidbluetoothscan.ui.view.fragment

import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.androidadvance.topsnackbar.TSnackbar
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentUserProfileBinding
import com.example.androidbluetoothscan.ui.viewmodel.UserProfileViewModel
import com.example.androidbluetoothscan.utils.getCurrentNavigate
import com.example.androidbluetoothscan.utils.guardLet
import com.example.androidbluetoothscan.utils.navigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserProfileFragment : Fragment() {

    private lateinit var binding: FragmentUserProfileBinding
    private val viewModel: UserProfileViewModel
            by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_profile, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getUserProfile()
        setListener()
        observeData()
    }

    private fun setListener() {
        binding.tvWebsite.setOnClickListener {
            val bundle = bundleOf(OpenUrlDialogFragment.KEY_URL to binding.tvWebsite.text.toString())
            if (getCurrentNavigate() == R.id.userProfileFragment)
                navigate(R.id.action_userProfileFragment_to_openUrlDialogFragment,bundle)
        }

        binding.btnGraphQl.setOnClickListener {
            if (getCurrentNavigate() == R.id.userProfileFragment)
                navigate(R.id.action_userProfileFragment_to_graphQlFragment)
        }
    }

    private fun observeData() {
        viewModel.error.observe(viewLifecycleOwner, Observer {
            val (apiError) = guardLet(it) {
                return@Observer
            }

            val snackBar = TSnackbar.make(
                binding.parentLayout,
                apiError.message.toString(),
                TSnackbar.LENGTH_LONG
            )
            snackBar.view.setBackgroundColor(Color.RED)
            val tvSnackBar =
                snackBar.view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            tvSnackBar.maxLines = 3
            tvSnackBar.setTextColor(Color.WHITE)
            tvSnackBar.layoutParams = params
            snackBar.setMaxWidth(Resources.getSystem().displayMetrics.widthPixels)
            snackBar.show()

        })
    }

}