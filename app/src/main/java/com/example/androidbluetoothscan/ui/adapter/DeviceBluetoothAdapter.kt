package com.example.androidbluetoothscan.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.ItemBluetoothDeviceBinding
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth

class DeviceBluetoothAdapter(private val devices: ArrayList<DeviceBluetooth>) :
    RecyclerView.Adapter<DeviceBluetoothAdapter.MainViewHolder>() {

    class MainViewHolder(private val binding: ItemBluetoothDeviceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            device: DeviceBluetooth
        ) {
            binding.viewModel = device
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return getItemView(parent)
    }

    private fun getItemView(parent: ViewGroup): MainViewHolder {
        val binding: ItemBluetoothDeviceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_bluetooth_device,
            parent,
            false
        )
        return MainViewHolder(binding)
    }

    override fun getItemCount(): Int = devices.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(devices[position])
    }

    fun addDevices(devices: List<DeviceBluetooth>) {
        this.devices.apply {
            clear()
            addAll(devices)
            notifyDataSetChanged()
        }
    }
}