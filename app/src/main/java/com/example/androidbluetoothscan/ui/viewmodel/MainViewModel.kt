package com.example.androidbluetoothscan.ui.viewmodel

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.services.BluetoothBroadcastReceiver
import com.example.androidbluetoothscan.services.BluetoothLeService
import kotlinx.coroutines.Job

class MainViewModel @ViewModelInject constructor() : ViewModel() {

    private var parentJob = Job()

    private var _listDevices = MutableLiveData<MutableList<DeviceBluetooth>>()
    val listDevices: LiveData<MutableList<DeviceBluetooth>> get() = _listDevices

    private var _isScanning = MutableLiveData<Boolean>()
    val isScanning: LiveData<Boolean> get() = _isScanning

    private var _needEnableBluetooth = MutableLiveData<Boolean>()
    val needEnableBluetooth: LiveData<Boolean> get() = _needEnableBluetooth

    private var _numberDevice = MutableLiveData<String>()
    val numberDevice: LiveData<String> get() = _numberDevice

    var scanBluetoothService: BluetoothLeService? = null
    var bluetoothBroadcastReceiver: BluetoothBroadcastReceiver = BluetoothBroadcastReceiver()

    private var _statusChangeBluetooth = MutableLiveData<Boolean>()
    val statusChangeBluetooth: LiveData<Boolean> get() = _statusChangeBluetooth

    private var _progressRssi = MutableLiveData<String>()
    val progressRssi: LiveData<String> get() = _progressRssi

    private var _supportBluetooth = MutableLiveData<Boolean>()
    val supportBluetooth: LiveData<Boolean> get() = _supportBluetooth

    init {

        bluetoothBroadcastReceiver.statusBluetoothChange = {
            _statusChangeBluetooth.value = it
        }
    }

    val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(
            componentName: ComponentName,
            service: IBinder
        ) {
            scanBluetoothService =
                (service as BluetoothLeService.ScanBluetoothBinder).getScanBluetoothService()

            scanBluetoothService?.run {
                startScan()
                changeDataCallBack = { it ->
                    _listDevices.postValue(it.sortedByDescending { it.rssi }.toMutableList())
                }
                statusScanCallBack = {
                    this@MainViewModel._isScanning.postValue(it)
                }
                needEnableBluetoothCallBack = {
                    this@MainViewModel._needEnableBluetooth.postValue(it)
                }
                statusSupportBluetoothCallBack = {
                    this@MainViewModel._supportBluetooth.postValue(it)
                }
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {

        }
    }

    fun stopScan() {
        scanBluetoothService?.stopScan()
    }

    fun scanOrStopBluetooth() {
        if (_isScanning.value == true) {
            stopScan()
        } else {
            startService()
        }
    }

    fun startService() {
        scanBluetoothService?.startScan()
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    fun setStatusChangeBluetooth(value: Boolean) {
        _statusChangeBluetooth.value = value
    }

    fun setEnableBluetooth(value: Boolean) {
        _needEnableBluetooth.value = value
    }

    fun setNumberDevice(value: String) {
        _numberDevice.value = value
    }

    fun setProgressRssiValue(value: String) {
        _progressRssi.value = value
    }

}