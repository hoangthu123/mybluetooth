package com.example.androidbluetoothscan.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidbluetoothscan.services.BluetoothBroadcastReceiver
import javax.inject.Inject

class LaunchViewModel @ViewModelInject constructor() :
    ViewModel() {

    val bluetoothBroadcastReceiver: BluetoothBroadcastReceiver = BluetoothBroadcastReceiver()
    private var _statusChangeBluetooth = MutableLiveData<Boolean>()
    val statusChangeBluetooth: LiveData<Boolean> get() = _statusChangeBluetooth

    init {
        bluetoothBroadcastReceiver.statusBluetoothChange = {
            _statusChangeBluetooth.value = it
        }
    }

    fun setStatusChangeBluetooth(value: Boolean) {
        _statusChangeBluetooth.value = value
    }
}