package com.example.androidbluetoothscan.ui.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentScanHistoryDeviceBinding
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.ui.adapter.DeviceBluetoothAdapter
import com.example.androidbluetoothscan.ui.viewmodel.ScanHistoryDeviceViewModel
import com.example.androidbluetoothscan.utils.navigateUp
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ScanHistoryDeviceFragment : Fragment() {

    private lateinit var binding: FragmentScanHistoryDeviceBinding
    private lateinit var adapter: DeviceBluetoothAdapter
    private val viewModel: ScanHistoryDeviceViewModel
            by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan_history_device, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        setListener()
        observeData()
    }

    private fun initView() {
        adapter = DeviceBluetoothAdapter(arrayListOf())
        binding.rcvDevices.adapter = adapter
        setUpSeekBar()
    }

    private fun setListener() {
        binding.btnClose.setOnClickListener { navigateUp() }
    }

    private fun observeData() {
        viewModel.devices.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setDataAdapter(it)
            }
        })
    }

    private fun setDataAdapter(it: MutableList<DeviceBluetooth>) {
        adapter.addDevices(it)
        viewModel.setNumberDevice("${it.size} ${getString(R.string.label_devices_peripherals)}")
    }

    private fun setUpSeekBar() {
        binding.sbRssi.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                var progress = p0?.progress ?: 0
                val devices: MutableList<DeviceBluetooth>
                if (progress != 0) {
                    progress *= -1
                    devices = viewModel.devices.value?.filter { s -> (s.rssi?:0) <= progress }
                        ?.toMutableList() ?: mutableListOf()
                } else {
                    devices = viewModel.devices.value ?: mutableListOf()
                }

                setDataAdapter(devices)
                viewModel.setProgressRssiValue("${resources.getString(R.string.label_rssi)}: $progress")
            }
        })
        viewModel.setProgressRssiValue("${resources.getString(R.string.label_rssi)}: ${binding.sbRssi.progress}")
    }

}