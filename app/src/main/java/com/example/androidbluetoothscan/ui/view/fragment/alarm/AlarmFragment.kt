package com.example.androidbluetoothscan.ui.view.fragment.alarm

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentAlarmBinding
import com.example.androidbluetoothscan.services.alarm.AlarmReceiver
import com.example.androidbluetoothscan.services.alarm.AlarmSoundService
import com.example.androidbluetoothscan.services.alarm.JobSchedulerService.Companion.NOTIFICATION_ID
import com.example.androidbluetoothscan.ui.view.DashboardActivity
import java.util.*


class AlarmFragment : Fragment() {

    //Pending intent instance
    private var pendingIntent: PendingIntent? = null

    //Alarm Request Code
    private val ALARM_REQUEST_CODE = 133

    private lateinit var binding: FragmentAlarmBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alarm, container, false)

        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        /* Retrieve a PendingIntent that will perform a broadcast */
        val alarmIntent = Intent(
            activity as DashboardActivity,
            AlarmReceiver::class.java
        )

        pendingIntent = PendingIntent.getBroadcast(
            activity as DashboardActivity,
            ALARM_REQUEST_CODE,
            alarmIntent,
            0
        )

        binding.startAlarmButton.setOnClickListener {
            val getInterval =
                binding.inputIntervalTime.text.toString().trim { it <= ' ' }

            //check interval should not be empty and 0
            if (getInterval != "" && getInterval != "0")
                triggerAlarmManager(getTimeInterval(getInterval))
        }

        //set on click over stop alarm button
        binding.stopAlarmButton.setOnClickListener { //Stop alarm manager
            stopAlarmManager()
        }

    }

    //get time interval to trigger alarm manager
    private fun getTimeInterval(getInterval: String): Int {
        val interval = getInterval.toInt()

        if (binding.secondsRadioButton.isChecked) return interval
        if (binding.minutesRadioButton.isChecked) return interval * 60
        if (binding.hoursRadioButton.isChecked) return interval * 60 * 60 //convert hours into seconds

        return 0
    }

    //Trigger alarm manager with entered time interval
    private fun triggerAlarmManager(alarmTriggerTime: Int) {
        // get a Calendar object with current time
        val cal: Calendar = Calendar.getInstance()
        // add alarmTriggerTime seconds to the calendar object
        cal.add(Calendar.SECOND, alarmTriggerTime)
        val manager =
            requireActivity().getSystemService(Context.ALARM_SERVICE) as AlarmManager //get instance of alarm manager
        manager.set(
            AlarmManager.RTC_WAKEUP,
            cal.timeInMillis,
            pendingIntent
        ) //set alarm manager with entered timer by converting into milliseconds
        Toast.makeText(
            requireContext(),
            "Alarm Set for $alarmTriggerTime seconds.",
            Toast.LENGTH_SHORT
        )
            .show()
    }

    //Stop/Cancel alarm manager
    private fun stopAlarmManager() {
        val manager = requireActivity().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        manager.cancel(pendingIntent) //cancel the alarm manager of the pending intent

        //Stop the Media Player Service to stop sound
        (activity as DashboardActivity).stopService(
            Intent(
                activity as DashboardActivity,
                AlarmSoundService::class.java
            )
        )

        //remove the notification from notification tray
        val notificationManager =
            requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
        Toast.makeText(requireContext(), "Alarm Canceled/Stop by User.", Toast.LENGTH_SHORT).show()
    }

}