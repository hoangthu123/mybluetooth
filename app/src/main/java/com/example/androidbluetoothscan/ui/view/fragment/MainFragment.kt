package com.example.androidbluetoothscan.ui.view.fragment

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentMainBinding
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.services.BluetoothLeService
import com.example.androidbluetoothscan.ui.adapter.DeviceBluetoothAdapter
import com.example.androidbluetoothscan.ui.dialog.MessageDialogFragment
import com.example.androidbluetoothscan.ui.view.DashboardActivity
import com.example.androidbluetoothscan.ui.viewmodel.MainViewModel
import com.example.androidbluetoothscan.utils.getCurrentNavigate
import com.example.androidbluetoothscan.utils.navigate

class MainFragment : Fragment() {

    companion object {
        private val REQUEST_ENABLE_BT = 1
    }

    private lateinit var binding: FragmentMainBinding
    private lateinit var adapter: DeviceBluetoothAdapter
    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        setListener()
        observeData()
    }

    private fun setListener() {
        binding.tvUserProfile.setOnClickListener { openUserProfileScreen() }

        binding.tvScanHistory.setOnClickListener { openScanHistoryScreen() }

        binding.tvOpenAlarm.setOnClickListener { openAlarmScreen() }
    }

    private fun openScanHistoryScreen() {
        if (getCurrentNavigate() == R.id.test2Fragment)
            navigate(R.id.action_test2Fragment_to_scanHistoryDeviceFragment)
    }

    private fun openAlarmScreen() {
        if (getCurrentNavigate() == R.id.test2Fragment)
            navigate(R.id.action_test2Fragment_to_alarmFragment)
    }

    private fun openUserProfileScreen() {
        if (getCurrentNavigate() == R.id.test2Fragment)
            navigate(R.id.action_test2Fragment_to_userProfileFragment)
    }

    private fun observeData() {
        viewModel.listDevices.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setDataAdapter(it)
            }
        })

        viewModel.needEnableBluetooth.observe(viewLifecycleOwner, Observer {
            requestEnableBluetooth()
        })

        viewModel.supportBluetooth.observe(viewLifecycleOwner, Observer {
            checkStateBluetooth(it)
        })

        viewModel.statusChangeBluetooth.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.startService()
            } else {
                requestEnableBluetooth()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as DashboardActivity).unbindService(viewModel.serviceConnection)
    }

    private fun startService() {
        val serviceIntent = Intent((activity as DashboardActivity), BluetoothLeService::class.java)
        (activity as DashboardActivity).startService(serviceIntent)
        bindService()
    }

    private fun bindService() {
        val serviceBindIntent =
            Intent((activity as DashboardActivity), BluetoothLeService::class.java)
        (activity as DashboardActivity).bindService(
            serviceBindIntent,
            viewModel.serviceConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    private fun initView() {
        adapter = DeviceBluetoothAdapter(arrayListOf())
        binding.rcvDevices.adapter = adapter
        startService()
        setUpSeekBar()
        (activity as DashboardActivity).bluetoothBroadcastReceiver.statusBluetoothChange = {
            viewModel.setStatusChangeBluetooth(it)
        }
    }

    private fun checkStateBluetooth(isSupport: Boolean) {
        if (isSupport) {
            viewModel.setStatusChangeBluetooth(isSupport)
        } else {
            val postalFragment =
                MessageDialogFragment().newInstance(getString(R.string.mess_not_support_bluetooth))
            postalFragment?.show(
                (activity as DashboardActivity).supportFragmentManager,
                MessageDialogFragment::class.java.name
            )
        }

    }

    private fun requestEnableBluetooth() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            viewModel.setEnableBluetooth(false)
        }

    }

    private fun setUpSeekBar() {
        binding.sbRssi.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                var progress = p0?.progress ?: 0
                val devices: MutableList<DeviceBluetooth>
                if (progress != 0) {
                    progress *= -1
                    devices = viewModel.listDevices.value?.filter { s -> (s.rssi ?: 0) <= progress }
                        ?.toMutableList() ?: mutableListOf()
                } else {
                    devices = viewModel.listDevices.value ?: mutableListOf()
                }

                setDataAdapter(devices)
                viewModel.setProgressRssiValue("${resources.getString(R.string.label_rssi)}: $progress")
            }
        })
        viewModel.setProgressRssiValue("${resources.getString(R.string.label_rssi)}: ${binding.sbRssi.progress}")
    }

    private fun setDataAdapter(it: MutableList<DeviceBluetooth>) {
        adapter.addDevices(it)
        viewModel.setNumberDevice("${it.size} ${getString(R.string.label_devices_peripherals)}")
    }

}