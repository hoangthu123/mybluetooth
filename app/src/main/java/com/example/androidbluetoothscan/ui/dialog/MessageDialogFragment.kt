package com.example.androidbluetoothscan.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.androidbluetoothscan.R
import com.example.androidbluetoothscan.databinding.FragmentMessageDialogBinding
import com.example.androidbluetoothscan.utils.BundleConstant.MESSAGE_DIALOG

class MessageDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentMessageDialogBinding

    fun newInstance(message: String): MessageDialogFragment? {
        val frag = MessageDialogFragment()
        val args = Bundle()
        args.putString(MESSAGE_DIALOG, message)
        frag.arguments = args
        return frag
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.apply {
            dialog.window?.setGravity(Gravity.CENTER)
            dialog.window!!.attributes.windowAnimations = R.style.AppDialogAnimation
        }

        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NORMAL, R.style.AppDialog);
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_message_dialog,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeData()

    }

    private fun initView() {
        arguments?.run {
            binding.tvMessage.text=this.get(MESSAGE_DIALOG) as String
        }
    }

    private fun observeData() {
        binding.btnClose.setOnClickListener { dismiss() }
    }
}
