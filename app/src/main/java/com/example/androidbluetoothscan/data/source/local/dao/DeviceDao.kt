package com.example.androidbluetoothscan.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.utils.DatabaseConstant.AMENITY_BLUETOOTH_DEVICE

@Dao
interface DeviceDao {

    @Query("SELECT * FROM $AMENITY_BLUETOOTH_DEVICE")
    fun getListDevices(): MutableList<DeviceBluetooth>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(amenity: DeviceBluetooth)

}