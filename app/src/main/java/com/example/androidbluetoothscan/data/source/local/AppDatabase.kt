package com.example.androidbluetoothscan.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.androidbluetoothscan.data.source.local.dao.DeviceDao
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.utils.DatabaseConstant.DATABASE_VERSION

@Database(entities = [DeviceBluetooth::class], version = DATABASE_VERSION, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val deviceDao: DeviceDao

}