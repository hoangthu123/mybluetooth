package com.example.androidbluetoothscan.data.repository

import com.example.androidbluetoothscan.data.source.local.AppDatabase
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import javax.inject.Inject

class DeviceRepositoryImp @Inject constructor(private val appDatabase: AppDatabase) :
    DeviceRepository {

    override fun insertDevice(deviceBluetooth: DeviceBluetooth) {
        val devices = appDatabase.deviceDao.getListDevices()
        if (devices.isNullOrEmpty()) {
            appDatabase.deviceDao.insert(deviceBluetooth)
        } else {
            val itemExist =
                devices.filter { item -> item.address == deviceBluetooth.address }
            if (itemExist.isNullOrEmpty()) {
                appDatabase.deviceDao.insert(deviceBluetooth)
            }
        }
    }

    override fun getDevicesNormal(): MutableList<DeviceBluetooth> {
        return appDatabase.deviceDao.getListDevices()
    }

}