package com.example.androidbluetoothscan.data.source.remote

import com.example.androidbluetoothscan.domain.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiService {

    @Headers(
        "Accept: application/json",
        "User-Agent: AndroidBluetoothScan",
        "Cache-Control: max-age=640000"
    )
    @GET("mocking/api/v1/links/6b4d76c6-59e1-462d-b0ec-a2034c899983/user-info")
    fun getUserProfile(): Single<User>
}
