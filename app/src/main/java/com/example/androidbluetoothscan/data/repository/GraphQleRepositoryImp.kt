package com.example.androidbluetoothscan.data.repository

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.androidbluetoothscan.FindQuery
import com.example.androidbluetoothscan.domain.repository.GraphQlRepository
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class GraphQleRepositoryImp @Inject constructor(
    private val apolloClient: ApolloClient
) :
    GraphQlRepository {

    override suspend fun getDataGraphQl(): String {
        return suspendCoroutine { continuation ->
            apolloClient.query(
                FindQuery    //From the auto generated class
                    .builder()
                    .name("S3_AndroidBluetoothScan_ThuHoang") //Passing required arguments
                    .owner("s3corp-github") //Passing required arguments
                    .build())
                .enqueue(object : ApolloCall.Callback<FindQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        continuation.resumeWithException(e)
                    }

                    override fun onResponse(response: Response<FindQuery.Data>) {
                        continuation.resume(response.data()?.repository().toString())
                    }
                })


        }
    }


}