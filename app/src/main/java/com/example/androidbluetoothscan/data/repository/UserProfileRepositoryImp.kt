package com.example.androidbluetoothscan.data.repository

import com.example.androidbluetoothscan.data.source.remote.ApiService
import com.example.androidbluetoothscan.domain.model.User
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import io.reactivex.Single
import javax.inject.Inject

class UserProfileRepositoryImp @Inject constructor(private val retrofitService: ApiService) :
    UserProfileRepository {

    override fun getUserProfile(): Single<User> {
        return retrofitService.getUserProfile()
    }


}