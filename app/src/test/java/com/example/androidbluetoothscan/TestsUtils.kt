package com.example.androidbluetoothscan

import com.example.androidbluetoothscan.domain.model.Address
import com.example.androidbluetoothscan.domain.model.Company
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.model.Geo
import com.example.androidbluetoothscan.domain.model.User

class TestsUtils {

    companion object {

        fun mockUser(): User {

            val geo = Geo(
                lat = "-37.3159",
                lng = "81.1496"
            )

            val address = Address(
                street = "180 Clemenceau Ave",
                building = "Haw Par Centre",
                unit = "#03-01/04",
                city = "Singapore",
                zipcode = "239922",
                geo = geo
            )
            val company = Company("SP Digital")

            return User(
                id = 1,
                name = "Leanne Graham",
                username = "Bret",
                email = "Sincere@something.sg",
                avatar = "https://api.adorable.io/avatars/285/Sincere@spdigital.sg.png",
                address = address,
                phone = "+6512345678",
                website = "https://www.spdigital.io",
                company = company
            )
        }

        fun mockDevicesBluetooth(): MutableList<DeviceBluetooth> {

            return (0..4).map {
                DeviceBluetooth(
                    id = it,
                    address = "Device $it",
                    name = "",
                    rssi = 1,
                    bondState = 1,
                    type = 1,
                    major = 1
                )
            }.toMutableList()
        }
    }

}