package com.example.androidbluetoothscan.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.TestsUtils.Companion.mockDevicesBluetooth
import com.example.androidbluetoothscan.data.repository.DeviceRepositoryImp
import com.example.androidbluetoothscan.data.source.local.AppDatabase
import com.example.androidbluetoothscan.data.source.local.dao.DeviceDao
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.*

class DeviceRepositoryImpMockkTests {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    private lateinit var appDatabase: AppDatabase

    @RelaxedMockK
    private lateinit var deviceRepository: DeviceRepository

    @RelaxedMockK
    private lateinit var dao: DeviceDao

    @Before
    fun before() {
        MockKAnnotations.init(this)
        every {appDatabase.deviceDao } returns dao
        deviceRepository = DeviceRepositoryImp(appDatabase)
    }

    @Test
    fun giveArrayOfDeviceBluetoothObjectWithFiveElements_whenAssignTheListOfBluetoothDevicesToRoom_thenDeviceBluetoothObjectArrayWithFiveElementsWillBeReturned() {
        val devices = mockDevicesBluetooth()
        every {dao.getListDevices() } returns devices
        Assert.assertEquals(5, deviceRepository.getDevicesNormal().size)
    }

    @Test
    fun giveEmptyArrayOfDeviceBluetoothObject_whenAssignTheListOfBluetoothDevicesToRoom_thenEmptyArrayOfDeviceBluetoothObjectWillBeReturned() {
        val devices =  mutableListOf<DeviceBluetooth>()
        every {dao.getListDevices() } returns devices
        Assert.assertEquals(0, deviceRepository.getDevicesNormal().size)
    }
    @Test
    fun `test insert new device when db empty`() {
        val devices =  mockDevicesBluetooth()
        deviceRepository.insertDevice(devices[0])
        verify(exactly = 1) {  dao.insert(devices[0]) }

    }

    @Test
    fun `test insert new device when db not empty`() {
        val devices =  mockDevicesBluetooth()
        every {dao.getListDevices() } returns devices
        val data = DeviceBluetooth(
            id = 5,
            address = "Device 5",
            name = "",
            rssi = 1,
            bondState = 1,
            type = 1,
            major = 1
        )
        deviceRepository.insertDevice(data)
        verify(exactly = 1) {  dao.insert(data) }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}

