package com.example.androidbluetoothscan.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.TestsUtils.Companion.mockDevicesBluetooth
import com.example.androidbluetoothscan.data.repository.DeviceRepositoryImp
import com.example.androidbluetoothscan.data.source.local.AppDatabase
import com.example.androidbluetoothscan.data.source.local.dao.DeviceDao
import com.example.androidbluetoothscan.domain.model.DeviceBluetooth
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class DeviceRepositoryImpTests {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var appDatabase: AppDatabase

    @Mock
    private lateinit var deviceRepository: DeviceRepository

    @Mock
    private lateinit var dao: DeviceDao

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(appDatabase.deviceDao).thenReturn(dao)
        deviceRepository = DeviceRepositoryImp(appDatabase)
    }

    @Test
    fun giveArrayOfDeviceBluetoothObjectWithFiveElements_whenAssignTheListOfBluetoothDevicesToRoom_thenDeviceBluetoothObjectArrayWithFiveElementsWillBeReturned() {
        val devices = mockDevicesBluetooth()
        Mockito.`when`(dao.getListDevices()).thenReturn(devices)
        Assert.assertEquals(5, deviceRepository.getDevicesNormal().size)
    }

    @Test
    fun giveEmptyArrayOfDeviceBluetoothObject_whenAssignTheListOfBluetoothDevicesToRoom_thenEmptyArrayOfDeviceBluetoothObjectWillBeReturned() {
        val devices =  mutableListOf<DeviceBluetooth>()
        Mockito.`when`(dao.getListDevices()).thenReturn(devices)
        Assert.assertEquals(0, deviceRepository.getDevicesNormal().size)
    }
    @Test
    fun `test insert new device when db empty`() {
        val devices =  mockDevicesBluetooth()
        deviceRepository.insertDevice(devices[0])
        verify(dao, times(1)).insert(devices[0])

    }

    @Test
    fun `test insert new device when db not empty`() {
        val devices =  mockDevicesBluetooth()
        Mockito.`when`(dao.getListDevices()).thenReturn(devices)
        val data = DeviceBluetooth(
            id = 5,
            address = "Device 5",
            name = "",
            rssi = 1,
            bondState = 1,
            type = 1,
            major = 1
        )
        deviceRepository.insertDevice(data)
        verify(dao, times(1)).insert(data)
    }
}

