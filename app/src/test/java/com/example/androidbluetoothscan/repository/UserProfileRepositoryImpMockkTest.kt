package com.example.androidbluetoothscan.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.TestsUtils
import com.example.androidbluetoothscan.data.repository.UserProfileRepositoryImp
import com.example.androidbluetoothscan.data.source.remote.ApiService
import com.example.androidbluetoothscan.domain.model.User
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UserProfileRepositoryImpMockkTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var api: ApiService

    @RelaxedMockK
    private lateinit var movieRepository: UserProfileRepository

    @Before
    fun before() {
        MockKAnnotations.init(this)
        movieRepository = UserProfileRepositoryImp(api)
    }

    @Test
    fun giveObjectUserNotEmpty_whenCallGetUserProfileApi_thenObjectUserNotEmptyWillBeReturned() {
        val user =
            TestsUtils.mockUser()
        every {api.getUserProfile() } returns Single.just(user)
        movieRepository.getUserProfile().test()
            .assertComplete()
            .assertValue { movies -> movies.id != null }
    }

    @Test
    fun giveObjectUserIsEmpty_whenCallGetUserProfileApi_thenObjectUserEmptyWillBeReturned() {
        val user = User()
        every {api.getUserProfile() } returns Single.just(user)
        movieRepository.getUserProfile().test()
            .assertComplete()
            .assertValue { movies -> movies.id == null }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}