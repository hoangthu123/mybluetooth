package com.example.androidbluetoothscan.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.TestsUtils
import com.example.androidbluetoothscan.data.repository.UserProfileRepositoryImp
import com.example.androidbluetoothscan.data.source.remote.ApiService
import com.example.androidbluetoothscan.domain.model.User
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class UserProfileRepositoryImpTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var api: ApiService

    @Mock
    private lateinit var movieRepository: UserProfileRepository

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        movieRepository = UserProfileRepositoryImp(api)
    }

    @Test
    fun giveObjectUserNotEmpty_whenCallGetUserProfileApi_thenObjectUserNotEmptyWillBeReturned() {
        val user =
            TestsUtils.mockUser()
        Mockito.`when`(api.getUserProfile()).thenReturn(Single.just(user))
        movieRepository.getUserProfile().test()
            .assertComplete()
            .assertValue { movies -> movies.id != null }
    }

    @Test
    fun giveObjectUserIsEmpty_whenCallGetUserProfileApi_thenObjectUserEmptyWillBeReturned() {
        val user = User()
        Mockito.`when`(api.getUserProfile()).thenReturn(Single.just(user))
        movieRepository.getUserProfile().test()
            .assertComplete()
            .assertValue { movies -> movies.id == null }
    }

}