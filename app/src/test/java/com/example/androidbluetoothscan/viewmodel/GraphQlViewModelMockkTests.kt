package com.example.androidbluetoothscan.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.domain.repository.GraphQlRepository
import com.example.androidbluetoothscan.domain.usecase.GraphQlUseCase
import com.example.androidbluetoothscan.ui.viewmodel.GraphQlViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GraphQlViewModelMockkTests {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var graphQlRepository: GraphQlRepository

    @MockK
    private lateinit var graphQlUseCase: GraphQlUseCase

    private lateinit var graphQlViewModel: GraphQlViewModel

    @Before
    fun before() {
        Dispatchers.setMain(TestCoroutineDispatcher())
        MockKAnnotations.init(this)
        graphQlUseCase = GraphQlUseCase(graphQlRepository)
        graphQlViewModel = GraphQlViewModel(graphQlUseCase)
        coEvery {graphQlUseCase.getDataGraphQl() } returns ""
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun checkGraphQlAfterLoading() {
        graphQlViewModel.loadData()
        coEvery {graphQlUseCase.getDataGraphQl() } returns "Dummy"
        graphQlViewModel.data.test().map { it != null }
            .assertValue(true)
    }

    @Test
    fun loadGraphQlShowAndHideLoading() {
        graphQlViewModel.isLoad.test().map { it }
            .assertValue(true)
        graphQlViewModel.loadData()
        coEvery {graphQlUseCase.getDataGraphQl() } returns "Dummy"
        graphQlViewModel.isLoad.test().map { it }
            .assertValue(false)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        unmockkAll()
    }

}

