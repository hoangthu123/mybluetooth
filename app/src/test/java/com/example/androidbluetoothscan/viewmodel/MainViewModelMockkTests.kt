package com.example.androidbluetoothscan.viewmodel

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.ui.viewmodel.MainViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.*

class MainViewModelMockkTests {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var deviceRepository: DeviceRepository

    @MockK
    private lateinit var deviceProfileUseCase: DeviceUseCase

    @MockK
    private lateinit var mainViewModel: MainViewModel

    @Before
    fun before() {
        MockKAnnotations.init(this)
        deviceProfileUseCase = DeviceUseCase(deviceRepository)
        mainViewModel = MainViewModel()
    }

    @Test
    fun giveBooleanValueIsTrue_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsTrueWillBeReturn() {
        val status = true
        mainViewModel.setStatusChangeBluetooth(status)
        mainViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(true)
    }

    @Test
    fun giveBooleanValueIsFalse_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsFalseWillBeReturn() {
        val status = false
        mainViewModel.setStatusChangeBluetooth(status)
        mainViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(false)
    }

    @Test
    fun giveBooleanValueIsTrue_whenSetEnableBluetooth_thenNeedEnableBluetoothLiveDataIsTrueWillBeReturn() {
        val status = true
        mainViewModel.setEnableBluetooth(status)
        mainViewModel.needEnableBluetooth.test().map { it }
            .assertValue(true)
    }

    @Test
    fun giveBooleanValueIsFalse_whenSetEnableBluetooth_thenNeedEnableBluetoothLiveDataIsFalseWillBeReturn() {
        val status = false
        mainViewModel.setEnableBluetooth(status)
        mainViewModel.needEnableBluetooth.test().map { it }
            .assertValue(false)
    }

    @Test
    fun giveTextValueIsFourDevice_whenSetNumberDevice_thenNumberDeviceLiveDataIsFourDeviceWillBeReturn() {
        val number = "4 devices/peripherals"
        mainViewModel.setNumberDevice(number)
        mainViewModel.numberDevice.test().map { it }
            .assertValue("4 devices/peripherals")
    }

    @Test
    fun giveTextValueIsEmpty_whenSetNumberDevice_thenNumberDeviceLiveDataIsEmptyWillBeReturn() {
        val number = ""
        mainViewModel.setNumberDevice(number)
        mainViewModel.numberDevice.test().map { it }
            .assertValue("")
    }

    @Test
    fun giveTextValueIsRssiFive_whenSetProgressRssiValue_thenProgressRssiLiveDataIsRssiFiveWillBeReturn() {
        val status = "Rssi: 5"
        mainViewModel.setProgressRssiValue(status)
        mainViewModel.progressRssi.test().map { it }
            .assertValue("Rssi: 5")
    }

  @Test
    fun giveTextValueIsEmpty_whenSetProgressRssiValue_thenProgressRssiLiveDataIsEmptyillBeReturn() {
        val status = ""
        mainViewModel.setProgressRssiValue(status)
        mainViewModel.progressRssi.test().map { it }
            .assertValue("")
    }

    @Test
    fun `bluetooth received OFF Status`() {
        val intent = mockk<Intent>()
        val context = mockk<Context>()
        every {intent.action } returns "android.bluetooth.adapter.action.STATE_CHANGED"
        every {intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)} returns BluetoothAdapter.STATE_OFF
        mainViewModel.bluetoothBroadcastReceiver.onReceive(context, intent)
        mainViewModel.setStatusChangeBluetooth(true)
        Assert.assertEquals(true, mainViewModel.statusChangeBluetooth.value)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}

