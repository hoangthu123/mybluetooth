package com.example.androidbluetoothscan.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.ui.viewmodel.ScanHistoryDeviceViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ScanHistoryViewModelMockkTests {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var deviceRepository: DeviceRepository

    @MockK
    private lateinit var deviceProfileUseCase: DeviceUseCase

    @MockK
    private lateinit var scanHistoryDeviceViewModel: ScanHistoryDeviceViewModel

    @Before
    fun before() {
        MockKAnnotations.init(this)
        deviceProfileUseCase = DeviceUseCase(deviceRepository)
        scanHistoryDeviceViewModel = ScanHistoryDeviceViewModel(deviceProfileUseCase)
    }

    @Test
    fun giveTextValueIsFourDevice_whenSetNumberDevice_thenNumberDeviceLiveDataIsFourDeviceWillBeReturn() {
        val number = "4 devices/peripherals"
        scanHistoryDeviceViewModel.setNumberDevice(number)
        scanHistoryDeviceViewModel.numberDevice.test().map { it }
            .assertValue("4 devices/peripherals")
    }

    @Test
    fun giveTextValueIsEmpty_whenSetNumberDevice_thenNumberDeviceLiveDataIsEmptyWillBeReturn() {
        val number = ""
        scanHistoryDeviceViewModel.setNumberDevice(number)
        scanHistoryDeviceViewModel.numberDevice.test().map { it }
            .assertValue("")
    }

    @Test
    fun giveTextValueIsRssiFive_whenSetProgressRssiValue_thenProgressRssiLiveDataIsRssiFiveWillBeReturn() {
        val status = "Rssi: 5"
        scanHistoryDeviceViewModel.setProgressRssiValue(status)
        scanHistoryDeviceViewModel.progressRssi.test().map { it }
            .assertValue("Rssi: 5")
    }

    @Test
    fun giveTextValueIsEmpty_whenSetProgressRssiValue_thenProgressRssiLiveDataIsEmptyillBeReturn() {
        val status = ""
        scanHistoryDeviceViewModel.setProgressRssiValue(status)
        scanHistoryDeviceViewModel.progressRssi.test().map { it }
            .assertValue("")
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}

