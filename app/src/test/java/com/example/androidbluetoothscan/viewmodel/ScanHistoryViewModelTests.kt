package com.example.androidbluetoothscan.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.domain.repository.DeviceRepository
import com.example.androidbluetoothscan.domain.usecase.DeviceUseCase
import com.example.androidbluetoothscan.ui.viewmodel.ScanHistoryDeviceViewModel
import com.jraska.livedata.test
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class ScanHistoryViewModelTests {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var deviceRepository: DeviceRepository

    private lateinit var deviceProfileUseCase: DeviceUseCase

    private lateinit var scanHistoryDeviceViewModel: ScanHistoryDeviceViewModel

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        deviceProfileUseCase = DeviceUseCase(deviceRepository)
        scanHistoryDeviceViewModel = ScanHistoryDeviceViewModel(deviceProfileUseCase)
    }

    @Test
    fun giveTextValueIsFourDevice_whenSetNumberDevice_thenNumberDeviceLiveDataIsFourDeviceWillBeReturn() {
        val number = "4 devices/peripherals"
        scanHistoryDeviceViewModel.setNumberDevice(number)
        scanHistoryDeviceViewModel.numberDevice.test().map { it }
            .assertValue("4 devices/peripherals")
    }

    @Test
    fun giveTextValueIsEmpty_whenSetNumberDevice_thenNumberDeviceLiveDataIsEmptyWillBeReturn() {
        val number = ""
        scanHistoryDeviceViewModel.setNumberDevice(number)
        scanHistoryDeviceViewModel.numberDevice.test().map { it }
            .assertValue("")
    }

    @Test
    fun giveTextValueIsRssiFive_whenSetProgressRssiValue_thenProgressRssiLiveDataIsRssiFiveWillBeReturn() {
        val status = "Rssi: 5"
        scanHistoryDeviceViewModel.setProgressRssiValue(status)
        scanHistoryDeviceViewModel.progressRssi.test().map { it }
            .assertValue("Rssi: 5")
    }

  @Test
    fun giveTextValueIsEmpty_whenSetProgressRssiValue_thenProgressRssiLiveDataIsEmptyillBeReturn() {
        val status = ""
        scanHistoryDeviceViewModel.setProgressRssiValue(status)
        scanHistoryDeviceViewModel.progressRssi.test().map { it }
            .assertValue("")
    }

}

