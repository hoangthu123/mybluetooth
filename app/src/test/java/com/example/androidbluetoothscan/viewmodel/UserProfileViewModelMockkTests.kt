package com.example.androidbluetoothscan.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.TestsUtils
import com.example.androidbluetoothscan.domain.repository.UserProfileRepository
import com.example.androidbluetoothscan.domain.usecase.UserProfileUseCase
import com.example.androidbluetoothscan.ui.viewmodel.UserProfileViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.Callable

class UserProfileViewModelMockkTests {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var userProfileRepository: UserProfileRepository

    @MockK
    private lateinit var userProfileUseCase: UserProfileUseCase

    @MockK
    private lateinit var userProfileViewModel: UserProfileViewModel

    @Before
    fun before() {
        MockKAnnotations.init(this)
        userProfileUseCase = UserProfileUseCase(userProfileRepository)
        userProfileViewModel = UserProfileViewModel(userProfileUseCase)
        every {userProfileUseCase.buildUseCaseSingle() } returns Single.just(TestsUtils.mockUser())
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { t: Callable<Scheduler?>? -> Schedulers.trampoline() }
    }

    @Test
    fun checkUserProfileDataAfterLoading() {
        userProfileViewModel.getUserProfile()
        every {userProfileUseCase.buildUseCaseSingle() } returns Single.just(TestsUtils.mockUser())
        userProfileViewModel.userProfile.test().map { it.id != null }
            .assertValue(true)
    }

    @Test
    fun loadUserProfileShowAndHideLoading() {
        userProfileViewModel.isLoad.test().map { it }
            .assertValue(true)
        userProfileViewModel.getUserProfile()
        every {userProfileUseCase.buildUseCaseSingle() } returns Single.just(TestsUtils.mockUser())
        userProfileViewModel.isLoad.test().map { it }
            .assertValue(false)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}

