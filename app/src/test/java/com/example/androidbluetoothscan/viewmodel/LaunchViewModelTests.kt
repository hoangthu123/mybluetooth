package com.example.androidbluetoothscan.viewmodel

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.ui.viewmodel.LaunchViewModel
import com.jraska.livedata.test
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class LaunchViewModelTests {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var launchViewModel: LaunchViewModel

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        launchViewModel = LaunchViewModel()
    }

    @Test
    fun giveBooleanValueIsTrue_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsTrueWillBeReturn() {
        val status = true
        launchViewModel.setStatusChangeBluetooth(status)
        launchViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(true)
    }

    @Test
    fun giveBooleanValueIsFalse_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsFalseWillBeReturn() {
        val status = false
        launchViewModel.setStatusChangeBluetooth(status)
        launchViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(false)
    }

    @Test
    fun `bluetooth received OFF Status`() {
        val intent = mock(Intent::class.java)
        `when`(intent.action).thenReturn("android.bluetooth.adapter.action.STATE_CHANGED")
        `when`(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)).thenReturn(BluetoothAdapter.STATE_OFF)
        launchViewModel.setStatusChangeBluetooth(true)
        launchViewModel.bluetoothBroadcastReceiver.onReceive(mock(Context::class.java), intent)
        Assert.assertEquals(false, launchViewModel.statusChangeBluetooth.value)
    }
}

