package com.example.androidbluetoothscan.viewmodel

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidbluetoothscan.ui.viewmodel.LaunchViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LaunchViewModelMockkTests {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    private lateinit var launchViewModel: LaunchViewModel

    @Before
    fun before() {
        MockKAnnotations.init(this)
        launchViewModel = LaunchViewModel()
    }

    @Test
    fun giveBooleanValueIsTrue_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsTrueWillBeReturn() {
        val status = true
        launchViewModel.setStatusChangeBluetooth(status)
        launchViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(true)
    }

    @Test
    fun giveBooleanValueIsFalse_whenSetStatusChangeBluetooth_thenStatusChangeBluetoothLiveDataIsFalseWillBeReturn() {
        val status = false
        launchViewModel.setStatusChangeBluetooth(status)
        launchViewModel.statusChangeBluetooth.test().map { it }
            .assertValue(false)
    }

    @Test
    fun `bluetooth received OFF Status`() {
        val intent =mockk<Intent>()
        val context =mockk<Context>()
        every {intent.action } returns "android.bluetooth.adapter.action.STATE_CHANGED"
        every {intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)} returns BluetoothAdapter.STATE_OFF
//        every {intent.action } returns 30 // when().thenReturn() in Mockito
        launchViewModel.bluetoothBroadcastReceiver.onReceive(context, intent)
        launchViewModel.setStatusChangeBluetooth(true)
        assertEquals(true, launchViewModel.statusChangeBluetooth.value)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

}

